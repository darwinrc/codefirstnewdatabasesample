﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstNewDatabaseSample
{
    public class Class1
    {
        public static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                // Create and save a new Blog 
                Console.Write("Enter a name for a new Blog: ");
                Console.WriteLine("YA NO SE USA!!!");
                var name = Console.ReadLine();

                Database.SetInitializer(new DatabaseInitializer());

                var blog = new Blog { Name = name };
                db.Blogs.Add(blog);
                db.SaveChanges();

                // Display all Blogs from the database 
                var query = from b in db.Blogs
                            orderby b.Name
                            select b;

                Console.WriteLine("All blogs in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine(item.Name);
                }

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Name { get; set; }

        public virtual List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public virtual Blog Blog { get; set; }
    }

    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>().ToTable("Blogs", schemaName: "laaurora");
            modelBuilder.Entity<Post>().ToTable("Posts", schemaName: "jardines");
        }  
    }

    public class DatabaseInitializer : CreateDatabaseIfNotExists<BloggingContext>
    {
        protected override void Seed(BloggingContext context)
        {
            var blog1 = new Blog { Name = "Blog 1" };
            var blog2 = new Blog { Name = "Blog 2" };
            var blog3 = new Blog { Name = "Blog 3" };
            var blog4 = new Blog { Name = "Blog 4" };
            var blog5 = new Blog { Name = "Blog 5" };

            var blog6 = new Blog { Name = "Blog 6" };
            var blog7 = new Blog { Name = "Blog 7" };
            var blog8 = new Blog { Name = "Blog 8" };
            var blog9 = new Blog { Name = "Blog 9" };
            var blog10 = new Blog { Name = "Blog 10" };


            context.Blogs.Add(blog1);
            context.Blogs.Add(blog2);
            context.Blogs.Add(blog3);
            context.Blogs.Add(blog4);
            context.Blogs.Add(blog5);

            context.Blogs.Add(blog6);
            context.Blogs.Add(blog7);
            context.Blogs.Add(blog8);
            context.Blogs.Add(blog9);
            context.Blogs.Add(blog10);

            context.SaveChanges();
        }
    }
}
