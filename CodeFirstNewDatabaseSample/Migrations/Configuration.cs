namespace CodeFirstNewDatabaseSample.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CodeFirstNewDatabaseSample.BloggingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CodeFirstNewDatabaseSample.BloggingContext";
        }

        protected override void Seed(CodeFirstNewDatabaseSample.BloggingContext context)
        {
            //  This method will be called after migrating to the latest version.
            context.Blogs.Add(new Blog { Name = "Blog 1" });
            context.Blogs.Add(new Blog { Name = "Blog 2" });
            context.Blogs.Add(new Blog { Name = "Blog 3" });
            context.Blogs.Add(new Blog { Name = "Blog 4" });
            context.Blogs.Add(new Blog { Name = "Blog 5" });

            context.Posts.AddOrUpdate(new Post { Blog = new Blog { Name = "blog" }, Content = "Hola" });

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
