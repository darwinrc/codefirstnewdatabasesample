namespace CodeFirstNewDatabaseSample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "laaurora.Blogs",
                c => new
                    {
                        BlogId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 1073741823, fixedLength: true),
                    })
                .PrimaryKey(t => t.BlogId);
            
            CreateTable(
                "jardines.Posts",
                c => new
                    {
                        PostId = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 1073741823, fixedLength: true),
                        Content = c.String(maxLength: 1073741823, fixedLength: true),
                        BlogId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PostId)
                .ForeignKey("laaurora.Blogs", t => t.BlogId, cascadeDelete: true)
                .Index(t => t.BlogId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("jardines.Posts", "BlogId", "laaurora.Blogs");
            DropIndex("jardines.Posts", new[] { "BlogId" });
            DropTable("jardines.Posts");
            DropTable("laaurora.Blogs");
        }
    }
}
